var config = require('../config'),
    gulp   = require('gulp');

gulp.task('watch', function() {
    gulp.watch(config.development.stylus + '**/*.styl', ['stylus']);
    gulp.watch(config.development.js + '**/*.js', ['jsHint', 'js']);
});
