(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.paginacaoNoticias = function() {
        var $controles  = $('#noticias-controls a'),
            $anteriores = $('#noticias-anteriores'),
            $proximas   = $('#noticias-proximas'),
            $links      = $('#noticias-links'),
            paginaAtual = $links.data('pagina'),
            url         = $('base').attr('href') + '/noticias-e-publicacoes';

        var carregaLinks = function(links, atual, ultima) {
            var linksHtml = '';

            for (var i = 0, length = links.length; i < length; i++) {
                linksHtml += '<a href="' + url + '/' + links[i].slug + '">&raquo; ' + links[i].titulo + '</a>';
            }

            $links.fadeOut('slow', function() {
                $links.html('').html(linksHtml).fadeIn();

                setTimeout(function() {
                    if (atual >= ultima) {
                        $anteriores.addClass('disabled');
                    } else {
                        $anteriores.removeClass('disabled');
                    }

                    if (atual == 1 || ultima < atual) {
                        $proximas.addClass('disabled');
                    } else {
                        $proximas.removeClass('disabled');
                    }
                }, 200);
            });
        };

        $controles.click(function(e) {
            e.preventDefault();

            if ($(this).hasClass('disabled')) return;

            var $controle = $(this);

            $controle.addClass('disabled');

            var proximaPagina = $(this).is('#noticias-anteriores') ? ++paginaAtual : --paginaAtual;

            $.get(url + '?page=' + proximaPagina, function(data) {
                carregaLinks(data.data, data.current_page, data.last_page);
                $links.data('pagina', proximaPagina);
                $controle.removeClass('disabled');
            });
        });
    };

    App.paginacaoBlog = function() {
        var $controles  = $('#posts-controls a'),
            $anteriores = $('#posts-anteriores'),
            $proximas   = $('#posts-proximas'),
            $links      = $('#posts-links'),
            paginaAtual = $links.data('pagina'),
            url         = $links.data('url'),
            linkUrl     = $('base').attr('href') + '/blog';

        var carregaLinks = function(links, atual, ultima) {
            var linksHtml = '';

            for (var i = 0, length = links.length; i < length; i++) {
                linksHtml += '<a href="' + linkUrl + '/' + links[i].categoria.slug + '/' + links[i].slug + '">&raquo; ' + links[i].titulo + ' <span>' + links[i].data + '</span></a>';
            }

            $links.fadeOut('slow', function() {
                $links.html('').html(linksHtml).fadeIn();

                setTimeout(function() {
                    if (atual >= ultima) {
                        $anteriores.addClass('disabled');
                    } else {
                        $anteriores.removeClass('disabled');
                    }

                    if (atual == 1 || ultima < atual) {
                        $proximas.addClass('disabled');
                    } else {
                        $proximas.removeClass('disabled');
                    }
                }, 200);
            });
        };

        $controles.click(function(e) {
            e.preventDefault();

            if ($(this).hasClass('disabled')) return;

            var $controle = $(this);

            $controle.addClass('disabled');

            var proximaPagina = $(this).is('#posts-anteriores') ? ++paginaAtual : --paginaAtual;

            $.get(url + '?page=' + proximaPagina, function(data) {
                carregaLinks(data.list.data, data.list.current_page, data.list.last_page);
                $links.data('pagina', proximaPagina);
                $controle.removeClass('disabled');
            });
        });
    };

    App.carregaMaisBlog = function() {
        var $handle    = $('#blog-carregar-mais'),
            $container = $('.blog-thumbs'),
            url        = $handle.data('url'),
            pagina     = 1;

        $handle.click(function(event) {
            event.preventDefault();

            if ($handle.hasClass('disabled')) return;

            $handle.addClass('disabled');

            $.get(url + '?page_thumbs=' + ++pagina, function(data) {
                $(data.thumbsHtml).appendTo($container).hide().fadeIn();
                if (data.thumbs.current_page >= data.thumbs.last_page) $handle.fadeOut();
                $handle.removeClass('disabled');
            });
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.envioComentario = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-comentario-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/blog-comentario',
            data: {
                post: $('#post').val(),
                nome: $('#nome').val(),
                email: $('#email').val(),
                comentario: $('#comentario').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                if (data.success) $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.init = function() {
        this.paginacaoNoticias();
        this.paginacaoBlog();
        this.carregaMaisBlog();
        $('#form-contato').on('submit', this.envioContato);
        $('#form-comentario').on('submit', this.envioComentario);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
