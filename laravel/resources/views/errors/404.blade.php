@extends('frontend.common.template', [
    'cabecalhoTexto'  => null,
    'cabecalhoImagem' => asset('assets/img/home/'.$home->cabecalho)
])

@section('content')

    <div class="not-found">
        <h1>Página não encontrada</h1>
    </div>

@endsection
