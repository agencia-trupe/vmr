@extends('frontend.common.template', [
    'cabecalhoTexto'  => $home->cabecalho_texto,
    'cabecalhoImagem' => asset('assets/img/home/'.$home->cabecalho)
])

@section('content')

    <div class="home">
        <div class="chamada">
            <div class="center">
                <div class="table">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/home/'.$home->chamada_imagem) }}" alt="">
                    </div>
                    <div class="texto">
                        <h2>{{ $home->chamada_titulo }}</h2>
                        <p>{!! $home->chamada_texto !!}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="atendimento">
            <div class="center">
                <h2>
                    <span>Atendimento jurídico</span>
                    prestado por especialistas em suas áreas de atuação.
                </h2>
                <a href="{{ route('atuacao') }}">SAIBA MAIS &raquo;</a>
            </div>
        </div>
    </div>

@endsection
