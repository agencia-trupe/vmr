@foreach($posts as $post)
<div class="blog-thumb">
    <img src="{{ asset('assets/img/blog/capas/'.$post->capa) }}" alt="">
    <div class="textos">
        <h1>{{ $post->titulo }}</h1>
        <div class="info">
            {{ Tools::formataData($post->data) }} <span></span> {{ mb_strtoupper($post->categoria->titulo) }}
        </div>
        <p>{!! $post->chamada !!}</p>
        <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}">LER MAIS &raquo;
        </a>
    </div>
</div>
@endforeach
