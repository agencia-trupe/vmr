@extends('frontend.common.template', [
    'cabecalhoTexto'  => 'Blog',
    'cabecalhoImagem' => asset('assets/img/blog/'.$blogCabecalho->cabecalho)
])

@section('content')

    <div class="blog">
        <div class="center">
            <div class="right">
                <div class="blog-thumbs">
                    @include('frontend.blog.thumbs', ['posts' => $postsThumbs])
                </div>
                @unless($postsThumbs->currentPage() >= $postsThumbs->lastPage())
                <a href="#" id="blog-carregar-mais" data-url="{{ route('blog', $categoria->slug) }}">
                    CARREGAR MAIS &raquo;
                </a>
                @endunless
            </div>
            <div class="left">
                <div class="categorias">
                    <h4>CATEGORIAS</h4>

                    <a href="{{ route('blog') }}" @if($categoria->slug == null) class="active" @endif>Todas</a>
                    @foreach($categorias as $c)
                    <a href="{{ route('blog', $c->slug) }}" @if($categoria->slug == $c->slug) class="active" @endif>{{ $c->titulo }}</a>
                    @endforeach
                </div>

                @if(!count($postsList))
                <div class="nenhuma">Nenhum post encontrado.</div>
                @endif

                <div class="links" id="posts-links" data-pagina="{{ $postsList->currentPage() }}" data-url="{{ route('blog', $categoria->slug) }}">
                    @foreach($postsList as $post)
                    <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}">
                        &raquo; {{ $post->titulo }} <span>{{ $post->data }}</span>
                    </a>
                    @endforeach
                </div>

                <div class="controls" id="posts-controls">
                    <a href="#" id="posts-anteriores" @if($postsList->currentPage() >= $postsList->lastPage()) class="disabled" @endif>&laquo; Anteriores</a>
                    <a href="#" id="posts-proximas" @if($postsList->currentPage() == 1 || $postsList->lastPage() < $postsList->currentPage()) class="disabled" @endif>Próximas &raquo;</a>
                </div>
            </div>
        </div>
    </div>

@endsection
