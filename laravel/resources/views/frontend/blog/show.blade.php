@extends('frontend.common.template', [
    'cabecalhoTexto'  => 'Blog',
    'cabecalhoImagem' => asset('assets/img/blog/'.$blogCabecalho->cabecalho)
])

@section('content')

    <div class="blog">
        <div class="center">
            <div class="right">
                <div class="blog-thumb">
                    <img src="{{ asset('assets/img/blog/capas/'.$post->capa) }}" alt="">
                    <div class="textos">
                        <h1>{{ $post->titulo }}</h1>
                        <div class="info">
                            {{ Tools::formataData($post->data) }} <span></span> {{ mb_strtoupper($post->categoria->titulo) }}
                        </div>
                        <div class="texto">
                            {!! $post->texto !!}
                        </div>
                    </div>
                </div>
                <div class="blog-comentarios">
                    @if(count($post->comentariosAprovados))
                    <h5>COMENTÁRIOS</h5>
                    @foreach($post->comentariosAprovados as $comentario)
                    <div class="comentario">
                        <p>{{ $comentario->comentario }}</p>
                        <span class="dados">{{ $comentario->nome }} &middot; {{ Tools::formataData($comentario->created_at) }}</span>
                    </div>
                    @endforeach
                    @endif

                    <form action="" id="form-comentario">
                        <h5>COMENTE</h5>
                        <input type="hidden" name="post" id="post" value="{{ $post->id }}">
                        <input type="text" name="nome" id="nome" placeholder="nome" required>
                        <input type="email" name="email" id="email" placeholder="e-mail" required>
                        <textarea name="comentario" id="comentario" placeholder="comentário" required></textarea>
                        <input type="submit" value="ENVIAR &raquo;">
                        <div id="form-comentario-response"></div>
                    </form>
                </div>
            </div>
            <div class="left">
                <div class="categorias">
                    <h4>CATEGORIAS</h4>

                    <a href="{{ route('blog') }}" @if($post->categoria->slug == null) class="active" @endif>Todas</a>
                    @foreach($categorias as $c)
                    <a href="{{ route('blog', $c->slug) }}" @if($post->categoria->slug == $c->slug) class="active" @endif>{{ $c->titulo }}</a>
                    @endforeach
                </div>

                @if(!count($postsList))
                <div class="nenhuma">Nenhum post encontrado.</div>
                @endif

                <div class="links" id="posts-links" data-pagina="{{ $postsList->currentPage() }}" data-url="{{ route('blog', $post->categoria->slug) }}">
                    @foreach($postsList as $post)
                    <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}">
                        &raquo; {{ $post->titulo }} <span>{{ $post->data }}</span>
                    </a>
                    @endforeach
                </div>

                <div class="controls" id="posts-controls">
                    <a href="#" id="posts-anteriores" @if($postsList->currentPage() >= $postsList->lastPage()) class="disabled" @endif>&laquo; Anteriores</a>
                    <a href="#" id="posts-proximas" @if($postsList->currentPage() == 1 || $postsList->lastPage() < $postsList->currentPage()) class="disabled" @endif>Próximas &raquo;</a>
                </div>
            </div>
        </div>
    </div>

@endsection
