    <header>
        <nav>
            <a href="{{ route('home') }}" @if(Route::currentRouteName() == 'home') class="active" @endif>HOME</a>
            <a href="{{ route('quem-somos') }}" @if(Route::currentRouteName() == 'quem-somos') class="active" @endif>QUEM SOMOS</a>
            <a href="{{ route('atuacao') }}" @if(Route::currentRouteName() == 'atuacao') class="active" @endif>ATUAÇÃO</a>
            <a href="{{ route('noticias') }}" @if(str_is('noticias*', Route::currentRouteName())) class="active" @endif>NOTÍCIAS <span>E PUBLICAÇÕES</span></a>
            <a href="{{ route('blog') }}" @if(str_is('blog*', Route::currentRouteName())) class="active" @endif>BLOG</a>
            <a href="{{ route('contato') }}" @if(Route::currentRouteName() == 'contato') class="active" @endif>CONTATO</a>
        </nav>
        <div class="center cabecalho-imagem @if(Route::currentRouteName() == 'home') home @endif">
            <div class="social">
            @foreach(['facebook', 'twitter', 'linkedin'] as $social)
                @if($contato->{$social})
                <a href="{{ $contato->{$social} }}" class="{{ $social }}" target="_blank">{{ $social }}</a>
                @endif
            @endforeach
            </div>
            <a href="{{ route('home') }}" class="logo">{{ config('site.title') }}</a>
            @if(isset($cabecalhoImagem))
            <div class="imagem" style="background-image:url('{{ $cabecalhoImagem }}')">
                <img src="{{ $cabecalhoImagem }}" alt="">
                @if($cabecalhoTexto)
                <h1>{!! $cabecalhoTexto !!}</h1>
                @endif
            </div>
            @else
            <div class="mapa">
                {!! $cabecalhoMapa !!}
                @if($cabecalhoTexto)
                <h1>{!! $cabecalhoTexto !!}</h1>
                @endif
            </div>
            @endif
        </div>
    </header>
