    <footer>
        <div class="chamada-contato">
            <div class="center">
                <h2>
                    ENTRE EM CONTATO:
                    <span>{{ $contato->telefone }}</span>
                </h2>
                <p>
                    {!! str_replace('<br />', ' &middot; ', $contato->endereco) !!}
                </p>
                <a href="{{ route('contato') }}">ENVIE-NOS UM E-MAIL</a>
            </div>
        </div>
        <div class="copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} Vieira de Melo Rudge Advocacia - Todos os direitos reservados
                    <span>|</span>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
