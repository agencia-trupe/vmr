@extends('frontend.common.template', [
    'cabecalhoTexto'  => 'Quem Somos',
    'cabecalhoImagem' => asset('assets/img/quem-somos/'.$quemSomos->cabecalho)
])

@section('content')

    <div class="quem-somos">
        <div class="textos">
            <div class="center">
                <div class="left">
                    <h2>{!! $quemSomos->chamada !!}</h2>
                </div>
                <div class="right">
                    {!! $quemSomos->texto !!}
                </div>
            </div>
        </div>
        <div class="profissionais">
            <div class="center">
                @foreach($profissionais as $profissional)
                <div class="profissional">
                    <div class="left">
                        <h3>{{ $profissional->nome }}</h3>
                        <img src="{{ asset('assets/img/profissionais/'.$profissional->foto) }}" alt="">
                    </div>
                    <div class="right">
                        {!! $profissional->texto !!}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
