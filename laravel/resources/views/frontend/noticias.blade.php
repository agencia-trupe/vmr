@extends('frontend.common.template', [
    'cabecalhoTexto'  => 'Notícias e Publicações',
    'cabecalhoImagem' => asset('assets/img/noticias/'.$noticiasCabecalho->cabecalho)
])

@section('content')

    <div class="noticias">
        <div class="center">
            <div class="right">
                @if($noticia)
                <h1>{{ $noticia->titulo }}</h1>
                <div class="info">
                    <span>autor: {{ $noticia->autor }}</span>
                    <span>{{ Tools::formataData($noticia->data) }}</span>
                </div>
                <div class="texto">
                    {!! $noticia->texto !!}
                </div>
                @endif
            </div>
            <div class="left">
                @if(!count($noticias))
                <div class="nenhuma">Nenhuma notícia encontrada.</div>
                @endif
                <div class="links" id="noticias-links" data-pagina="{{ $noticias->currentPage() }}">
                    @foreach($noticias as $noticia)
                    <a href="{{ route('noticias.show', $noticia->slug) }}">
                        &raquo; {{ $noticia->titulo }}
                    </a>
                    @endforeach
                </div>
                <div class="controls" id="noticias-controls">
                    <a href="#" id="noticias-anteriores" @if($noticias->currentPage() >= $noticias->lastPage()) class="disabled" @endif>&laquo; Anteriores</a>
                    <a href="#" id="noticias-proximas" @if($noticias->currentPage() == 1 || $noticias->lastPage() < $noticias->currentPage()) class="disabled" @endif>Próximas &raquo;</a>
                </div>
            </div>
        </div>
    </div>

@endsection
