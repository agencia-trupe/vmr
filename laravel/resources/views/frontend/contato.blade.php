@extends('frontend.common.template', [
    'cabecalhoTexto'  => 'Contato',
    'cabecalhoMapa'   => $contato->google_maps
])

@section('content')

    <div class="contato">
        <div class="center">
            <div class="left">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="endereco">{!! $contato->endereco !!}</p>
                <p class="amplitude">
                    <strong>AMPLITUDE NACIONAL</strong>
                    {!! $contato->amplitude_nacional !!}
                </p>
            </div>
            <div class="right">
                <form action="" id="form-contato" method="POST">
                    <p>FALE CONOSCO</p>
                    <input type="text" name="nome" id="nome" placeholder="nome" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                    <input type="submit" value="ENVIAR &raquo;">
                    <div id="form-contato-response"></div>
                </form>
            </div>
        </div>
    </div>

@endsection
