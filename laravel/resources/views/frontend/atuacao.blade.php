@extends('frontend.common.template', [
    'cabecalhoTexto'  => 'Atuação',
    'cabecalhoImagem' => asset('assets/img/atuacao/'.$atuacao->cabecalho)
])

@section('content')

    <div class="atuacao">
        <div class="textos">
            <div class="center">
                <div class="left">
                    <h2>{!! $atuacao->chamada !!}</h2>
                    <p>
                        <strong>Público alvo:</strong>
                        {!! $atuacao->publico_alvo !!}
                    </p>
                </div>
                <div class="right">
                    {!! $atuacao->texto !!}
                </div>
            </div>
        </div>
        <div class="imagens">
            <div class="center">
                @foreach(range(1,3) as $i)
                <img src="{{ asset('assets/img/atuacao/'.$atuacao->{'foto_'.$i}) }}" alt="">
                @endforeach
            </div>
        </div>
    </div>

@endsection
