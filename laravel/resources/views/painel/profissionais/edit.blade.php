@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Profissionais /</small> Editar Profissional</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.profissionais.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.profissionais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
