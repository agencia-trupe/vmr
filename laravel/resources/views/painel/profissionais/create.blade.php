@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Profissionais /</small> Adicionar Profissional</h2>
    </legend>

    {!! Form::open(['route' => 'painel.profissionais.store', 'files' => true]) !!}

        @include('painel.profissionais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
