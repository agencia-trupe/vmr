@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('foto', 'Foto') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/profissionais/'.$registro->foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('foto', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'profissionais']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.profissionais.index') }}" class="btn btn-default btn-voltar">Voltar</a>
