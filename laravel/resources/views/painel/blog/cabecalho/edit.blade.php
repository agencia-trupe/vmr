@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Blog /</small> Cabeçalho</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.blog.cabecalho.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.blog.cabecalho.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
