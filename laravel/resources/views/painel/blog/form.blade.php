@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('categoria_id', 'Categoria') !!}
    {!! Form::select('categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/blog/capas/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada', 'Chamada') !!}
    {!! Form::textarea('chamada', null, ['class' => 'form-control', 'style' => 'resize:none;height:180px']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'completo']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.blog.index') }}" class="btn btn-default btn-voltar">Voltar</a>
