@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Notícias /</small> Editar Cabeçalho</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.noticias.cabecalho.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.noticias-cabecalho.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
