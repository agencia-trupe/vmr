@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('cabecalho', 'Cabeçalho') !!}
    <img src="{{ url('assets/img/noticias/'.$registro->cabecalho) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('cabecalho', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
<a href="{{ route('painel.noticias.index') }}" class="btn btn-default btn-voltar">Voltar</a>
