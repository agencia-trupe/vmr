@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('cabecalho', 'Cabeçalho') !!}
            <img src="{{ url('assets/img/home/'.$registro->cabecalho) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('cabecalho', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('cabecalho_texto', 'Cabeçalho Texto') !!}
            {!! Form::textarea('cabecalho_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('chamada_imagem', 'Chamada Imagem') !!}
            <img src="{{ url('assets/img/home/'.$registro->chamada_imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('chamada_imagem', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_titulo', 'Chamada Título') !!}
            {!! Form::text('chamada_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_texto', 'Chamada Texto') !!}
            {!! Form::textarea('chamada_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
