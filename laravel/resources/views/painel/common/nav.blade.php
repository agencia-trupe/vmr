<ul class="nav navbar-nav">
    <li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.home.index') }}">Home</a>
    </li>
    <li @if(str_is('painel.quem-somos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.quem-somos.index') }}">Quem Somos</a>
    </li>
    <li @if(str_is('painel.profissionais*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.profissionais.index') }}">Profissionais</a>
    </li>
    <li @if(str_is('painel.atuacao*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.atuacao.index') }}">Atuação</a>
    </li>
    <li @if(str_is('painel.noticias*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.noticias.index') }}">Notícias</a>
    </li>
	<li @if(str_is('painel.blog*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.blog.index') }}">Blog</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
