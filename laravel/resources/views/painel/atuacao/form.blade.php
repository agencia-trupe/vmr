@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('cabecalho', 'Cabeçalho') !!}
    <img src="{{ url('assets/img/atuacao/'.$registro->cabecalho) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('cabecalho', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada', 'Chamada') !!}
            {!! Form::textarea('chamada', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('publico_alvo', 'Público Alvo') !!}
            {!! Form::textarea('publico_alvo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('foto_1', 'Foto 1') !!}
            <img src="{{ url('assets/img/atuacao/'.$registro->foto_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('foto_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('foto_2', 'Foto 2') !!}
            <img src="{{ url('assets/img/atuacao/'.$registro->foto_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('foto_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('foto_3', 'Foto 3') !!}
            <img src="{{ url('assets/img/atuacao/'.$registro->foto_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('foto_3', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
