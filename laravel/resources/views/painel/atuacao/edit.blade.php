@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Atuação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.atuacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.atuacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
