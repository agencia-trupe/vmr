@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('cabecalho', 'Cabeçalho') !!}
    <img src="{{ url('assets/img/quem-somos/'.$registro->cabecalho) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('cabecalho', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada', 'Chamada') !!}
    {!! Form::textarea('chamada', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
