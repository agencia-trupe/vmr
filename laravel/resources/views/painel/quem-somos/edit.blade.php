@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Quem Somos</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.quem-somos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.quem-somos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
