<?php

use Illuminate\Database\Seeder;

class AtuacaoSeeder extends Seeder
{
    public function run()
    {
        DB::table('atuacao')->insert([
            'cabecalho' => '',
            'chamada' => '',
            'publico_alvo' => '',
            'texto' => '',
            'foto_1' => '',
            'foto_2' => '',
            'foto_3' => '',
        ]);
    }
}
