<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'cabecalho' => '',
            'chamada_imagem' => '',
            'chamada_titulo' => '',
            'chamada_texto' => '',
        ]);
    }
}
