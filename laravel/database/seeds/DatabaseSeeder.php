<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(BlogCabecalhoSeeder::class);
		$this->call(NoticiasCabecalhoSeeder::class);
		$this->call(AtuacaoSeeder::class);
		$this->call(QuemSomosSeeder::class);
		$this->call(HomeSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
