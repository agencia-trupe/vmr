<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogCabecalhoTable extends Migration
{
    public function up()
    {
        Schema::create('blog_cabecalho', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cabecalho');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('blog_cabecalho');
    }
}
