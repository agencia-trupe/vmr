<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtuacaoTable extends Migration
{
    public function up()
    {
        Schema::create('atuacao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cabecalho');
            $table->text('chamada');
            $table->text('publico_alvo');
            $table->text('texto');
            $table->string('foto_1');
            $table->string('foto_2');
            $table->string('foto_3');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('atuacao');
    }
}
