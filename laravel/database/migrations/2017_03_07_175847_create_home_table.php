<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cabecalho');
            $table->string('cabecalho_texto');
            $table->string('chamada_imagem');
            $table->string('chamada_titulo');
            $table->text('chamada_texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
