<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasCabecalhoTable extends Migration
{
    public function up()
    {
        Schema::create('noticias_cabecalho', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cabecalho');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('noticias_cabecalho');
    }
}
