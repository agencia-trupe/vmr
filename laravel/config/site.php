<?php

return [

    'name'        => 'VMR',
    'title'       => 'Vieira de Mello Rudge &middot; ADVOCACIA',
    'description' => 'Consultoria empresarial e Advocacia voltadas para o desenvolvimento completo do seu negócio.',
    'keywords'    => '',
    'share_image' => '',
    'analytics'   => null

];
