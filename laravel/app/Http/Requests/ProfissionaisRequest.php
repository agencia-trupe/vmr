<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfissionaisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'foto' => 'required|image',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['foto'] = 'image';
        }

        return $rules;
    }
}
