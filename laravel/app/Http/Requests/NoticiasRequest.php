<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NoticiasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data' => 'required',
            'titulo' => 'required',
            'slug' => '',
            'autor' => 'required',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
