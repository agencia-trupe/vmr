<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AtuacaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'cabecalho' => 'image',
            'chamada' => 'required',
            'publico_alvo' => 'required',
            'texto' => 'required',
            'foto_1' => 'image',
            'foto_2' => 'image',
            'foto_3' => 'image',
        ];
    }
}
