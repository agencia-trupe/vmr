<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NoticiasCabecalhoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'cabecalho' => 'required|image',
        ];
    }
}
