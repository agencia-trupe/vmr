<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'cabecalho' => 'image',
            'cabecalho_texto' => 'required',
            'chamada_imagem' => 'image',
            'chamada_titulo' => 'required',
            'chamada_texto' => 'required',
        ];
    }
}
