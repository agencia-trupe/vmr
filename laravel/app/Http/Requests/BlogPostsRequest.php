<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BlogPostsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'categoria_id' => 'required',
            'data' => 'required',
            'titulo' => 'required',
            'slug' => '',
            'capa' => 'required|image',
            'chamada' => 'required',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
