<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\BlogComentariosRequest;

use App\Models\BlogCategoria;
use App\Models\BlogPost;
use App\Models\BlogCabecalho;

class BlogController extends Controller
{
    public function index(BlogCategoria $categoria, Request $request)
    {
        $posts = (! $categoria->exists)
            ? BlogPost::ordenados()
            : BlogPost::ordenados()->categoria($categoria->id);

        if ($request->ajax()) {
            return response()->json([
                'list'       => $posts->with('categoria')->paginate(10),
                'thumbs'     => $posts->paginate(3, ['id'], 'page_thumbs'),
                'thumbsHtml' => view('frontend.blog.thumbs', [
                    'posts' => $posts->paginate(3, ['*'], 'page_thumbs')
                ])->render()
            ]);
        }

        $categorias    = BlogCategoria::ordenados()->select('slug', 'titulo')->get();
        $postsList     = $posts->paginate(10);
        $postsThumbs   = $posts->paginate(3, ['*'], 'page_thumbs');
        $blogCabecalho = BlogCabecalho::first();

        return view('frontend.blog.index', compact('categorias', 'postsList', 'postsThumbs', 'blogCabecalho', 'categoria'));
    }

    public function show(BlogCategoria $categoria, BlogPost $post)
    {
        $posts = (! $categoria->exists)
            ? BlogPost::ordenados()
            : BlogPost::ordenados()->categoria($categoria->id);

        $categorias    = BlogCategoria::ordenados()->select('slug', 'titulo')->get();
        $postsList     = $posts->paginate(10);
        $blogCabecalho = BlogCabecalho::first();

        return view('frontend.blog.show', compact('categorias', 'postsList', 'post', 'blogCabecalho'));
    }

    public function comentario(BlogComentariosRequest $request)
    {
        try {

            $post = BlogPost::findOrFail($request->get('post'));
            $post->comentarios()->create($request->except('post'));

            \Mail::send('emails.comentario', [
                'comentario' => $request->all(),
                'post'       => $post
            ], function($message) use ($request) {
                $message->to('blog@vmradvocacia.com.br', 'Vieira de Mello Rudge Advocacia')
                        ->subject('[NOVO COMENTÁRIO] Vieira de Mello Rudge Advocacia');
            });

        } catch (\Exception $e) {

            $response = [
                'message' => 'Ocorreu um erro. Tente novamente.'
            ];
            return response()->json($response);

        }

        $response = [
            'success' => true,
            'message' => 'Comentário enviado com sucesso. Sujeito à aprovação da moderação.'
        ];
        return response()->json($response);
    }
}
