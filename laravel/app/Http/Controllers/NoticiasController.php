<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Noticia;
use App\Models\NoticiasCabecalho;

class NoticiasController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax())
            return response()->json(Noticia::ordenados()->select('slug', 'titulo')->paginate(10));

        $noticias          = Noticia::ordenados()->paginate(10);
        $noticia           = Noticia::ordenados()->first();
        $noticiasCabecalho = NoticiasCabecalho::first();
        return view('frontend.noticias', compact('noticias', 'noticiasCabecalho', 'noticia'));
    }

    public function show(Noticia $noticia)
    {
        $noticias          = Noticia::ordenados()->paginate(10);
        $noticiasCabecalho = NoticiasCabecalho::first();
        return view('frontend.noticias', compact('noticias', 'noticiasCabecalho', 'noticia'));
    }
}
