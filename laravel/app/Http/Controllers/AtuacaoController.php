<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Atuacao;

class AtuacaoController extends Controller
{
    public function index()
    {
        $atuacao = Atuacao::first();
        return view('frontend.atuacao', compact('atuacao'));
    }
}
