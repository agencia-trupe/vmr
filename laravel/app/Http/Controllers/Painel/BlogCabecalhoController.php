<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BlogCabecalhoRequest;
use App\Http\Controllers\Controller;

use App\Models\BlogCabecalho;

class BlogCabecalhoController extends Controller
{
    public function index()
    {
        $registro = BlogCabecalho::first();

        return view('painel.blog.cabecalho.edit', compact('registro'));
    }

    public function update(BlogCabecalhoRequest $request, BlogCabecalho $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['cabecalho'])) $input['cabecalho'] = BlogCabecalho::upload_cabecalho();

            $registro->update($input);

            return redirect()->route('painel.blog.cabecalho.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
