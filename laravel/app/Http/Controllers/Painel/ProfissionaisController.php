<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProfissionaisRequest;
use App\Http\Controllers\Controller;

use App\Models\Profissional;

class ProfissionaisController extends Controller
{
    public function index()
    {
        $registros = Profissional::ordenados()->get();

        return view('painel.profissionais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.profissionais.create');
    }

    public function store(ProfissionaisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = Profissional::upload_foto();

            Profissional::create($input);
            return redirect()->route('painel.profissionais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Profissional $registro)
    {
        return view('painel.profissionais.edit', compact('registro'));
    }

    public function update(ProfissionaisRequest $request, Profissional $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = Profissional::upload_foto();

            $registro->update($input);
            return redirect()->route('painel.profissionais.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Profissional $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.profissionais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
