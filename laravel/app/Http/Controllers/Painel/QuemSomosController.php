<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\QuemSomosRequest;
use App\Http\Controllers\Controller;

use App\Models\QuemSomos;

class QuemSomosController extends Controller
{
    public function index()
    {
        $registro = QuemSomos::first();

        return view('painel.quem-somos.edit', compact('registro'));
    }

    public function update(QuemSomosRequest $request, QuemSomos $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['cabecalho'])) $input['cabecalho'] = QuemSomos::upload_cabecalho();

            $registro->update($input);

            return redirect()->route('painel.quem-somos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
