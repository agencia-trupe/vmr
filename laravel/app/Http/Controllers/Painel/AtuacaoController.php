<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AtuacaoRequest;
use App\Http\Controllers\Controller;

use App\Models\Atuacao;

class AtuacaoController extends Controller
{
    public function index()
    {
        $registro = Atuacao::first();

        return view('painel.atuacao.edit', compact('registro'));
    }

    public function update(AtuacaoRequest $request, Atuacao $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['cabecalho'])) $input['cabecalho'] = Atuacao::upload_cabecalho();
            if (isset($input['foto_1'])) $input['foto_1'] = Atuacao::upload_foto_1();
            if (isset($input['foto_2'])) $input['foto_2'] = Atuacao::upload_foto_2();
            if (isset($input['foto_3'])) $input['foto_3'] = Atuacao::upload_foto_3();

            $registro->update($input);

            return redirect()->route('painel.atuacao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
