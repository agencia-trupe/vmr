<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NoticiasCabecalhoRequest;
use App\Http\Controllers\Controller;

use App\Models\NoticiasCabecalho;

class NoticiasCabecalhoController extends Controller
{
    public function index()
    {
        $registro = NoticiasCabecalho::first();

        return view('painel.noticias-cabecalho.edit', compact('registro'));
    }

    public function update(NoticiasCabecalhoRequest $request, NoticiasCabecalho $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['cabecalho'])) $input['cabecalho'] = NoticiasCabecalho::upload_cabecalho();

            $registro->update($input);

            return redirect()->route('painel.noticias.cabecalho.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
