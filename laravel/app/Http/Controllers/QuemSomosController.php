<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\QuemSomos;
use App\Models\Profissional;

class QuemSomosController extends Controller
{
    public function index()
    {
        $quemSomos     = QuemSomos::first();
        $profissionais = Profissional::ordenados()->get();
        return view('frontend.quem-somos', compact('quemSomos', 'profissionais'));
    }
}
