<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('quem-somos', 'QuemSomosController@index')->name('quem-somos');
    Route::get('atuacao', 'AtuacaoController@index')->name('atuacao');
    Route::get('noticias-e-publicacoes', 'NoticiasController@index')->name('noticias');
    Route::get('noticias-e-publicacoes/{noticia_slug}', 'NoticiasController@show')->name('noticias.show');
    Route::get('blog/{blog_categoria_slug?}', 'BlogController@index')->name('blog');
    Route::get('blog/{blog_categoria_slug}/{blog_slug}', 'BlogController@show')->name('blog.show');
    Route::post('blog-comentario', 'BlogController@comentario');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('blog/cabecalho', 'BlogCabecalhoController', ['only' => ['index', 'update']]);
		Route::resource('blog/categorias', 'BlogCategoriasController');
        Route::resource('blog', 'BlogPostsController');
        Route::get('blog/{blog}/comentarios/{comentarios}/aprovacao', 'BlogComentariosController@aprovacao')->name('painel.blog.comentarios.aprovacao');
        Route::resource('blog.comentarios', 'BlogComentariosController');
		Route::resource('noticias/cabecalho', 'NoticiasCabecalhoController', ['only' => ['index', 'update']]);
		Route::resource('noticias', 'NoticiasController');
		Route::resource('atuacao', 'AtuacaoController', ['only' => ['index', 'update']]);
		Route::resource('profissionais', 'ProfissionaisController');
		Route::resource('quem-somos', 'QuemSomosController', ['only' => ['index', 'update']]);
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
