<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Profissional extends Model
{
    protected $table = 'profissionais';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_foto()
    {
        return CropImage::make('foto', [
            'width'  => 300,
            'height' => null,
            'path'   => 'assets/img/profissionais/'
        ]);
    }

}
