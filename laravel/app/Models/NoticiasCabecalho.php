<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class NoticiasCabecalho extends Model
{
    protected $table = 'noticias_cabecalho';

    protected $guarded = ['id'];

    public static function upload_cabecalho()
    {
        return CropImage::make('cabecalho', [
            'width'  => 600,
            'height' => 300,
            'path'   => 'assets/img/noticias/'
        ]);
    }

}
