<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class QuemSomos extends Model
{
    protected $table = 'quem_somos';

    protected $guarded = ['id'];

    public static function upload_cabecalho()
    {
        return CropImage::make('cabecalho', [
            'width'  => 600,
            'height' => 300,
            'path'   => 'assets/img/quem-somos/'
        ]);
    }

}
