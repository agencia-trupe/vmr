<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Atuacao extends Model
{
    protected $table = 'atuacao';

    protected $guarded = ['id'];

    public static function upload_cabecalho()
    {
        return CropImage::make('cabecalho', [
            'width'  => 600,
            'height' => 300,
            'path'   => 'assets/img/atuacao/'
        ]);
    }

    public static function upload_foto_1()
    {
        return CropImage::make('foto_1', [
            'width'  => 400,
            'height' => 270,
            'path'   => 'assets/img/atuacao/'
        ]);
    }

    public static function upload_foto_2()
    {
        return CropImage::make('foto_2', [
            'width'  => 400,
            'height' => 270,
            'path'   => 'assets/img/atuacao/'
        ]);
    }

    public static function upload_foto_3()
    {
        return CropImage::make('foto_3', [
            'width'  => 400,
            'height' => 270,
            'path'   => 'assets/img/atuacao/'
        ]);
    }

}
