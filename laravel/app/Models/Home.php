<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_cabecalho()
    {
        return CropImage::make('cabecalho', [
            'width'  => 600,
            'height' => 400,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_chamada_imagem()
    {
        return CropImage::make('chamada_imagem', [
            'width'  => 450,
            'height' => null,
            'path'   => 'assets/img/home/'
        ]);
    }

}
