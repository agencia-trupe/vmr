<?php

namespace App\Helpers;

class Tools
{

    public static function formataData($data)
    {
        $meses = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];

        list($dia, $mes, $ano) = explode('/', $data);

        return $dia . ' de ' . $meses[(int) $mes - 1] . ' de ' . $ano;
    }

}
