<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('comentarios', 'App\Models\BlogComentario');
		$router->model('blog', 'App\Models\BlogPost');
		$router->model('categorias', 'App\Models\BlogCategoria');
		$router->model('noticias', 'App\Models\Noticia');
		$router->model('atuacao', 'App\Models\Atuacao');
		$router->model('profissionais', 'App\Models\Profissional');
		$router->model('quem-somos', 'App\Models\QuemSomos');
		$router->model('home', 'App\Models\Home');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('cabecalho', function($id, $route, $model = null) {
            if (str_is('*noticias*', $route->getPrefix())) {
                $model = \App\Models\NoticiasCabecalho::find($id);
            } elseif (str_is('*blog*', $route->getPrefix())) {
                $model = \App\Models\BlogCabecalho::find($id);
            }

            return $model ?: \App::abort('404');
        });

        $router->bind('noticia_slug', function($value) {
            return \App\Models\Noticia::whereSlug($value)->first() ?: \App::abort('404');
        });
        $router->bind('blog_categoria_slug', function($value) {
            return \App\Models\BlogCategoria::whereSlug($value)->first() ?: \App::abort('404');
        });
        $router->bind('blog_slug', function($value) {
            return \App\Models\BlogPost::whereSlug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
